//
// Created by Fábio Albuquerque on 03/04/18.
//

#include <stdlib.h>
#include <stdio.h>
#include <zconf.h>

int main() {
    printf("\n Ponto 1: \n \n");
    ponto1();
    printf("\n Ponto 2: \n \n");
    ponto2();
    printf("\n Ponto 3: \n \n");
    ponto3();
    printf("\n Ponto 4.1: \n \n");
    ponto4_1();
    return 0;
}

int ponto1(){
    return system("ps xao pid,ppid,command | head");
}

int ponto2() {
    int return_value;
    return_value = system("ls -l /");
    return return_value; //executa o command ls -l no root directory
}

int ponto3(){
    pid_t child_pid;

    printf("The main program ID is %d \n", (int) getpid());

    child_pid = fork();
    if (child_pid != 0){
        printf("This is the parent process, with id %d \n", (int) getpid());
        printf("The child's process ID is %d \n", (int) child_pid);
    }
    else
        printf("This is the child process, with id %d \n", (int) getpid());
}

int ponto4_1(){
    int i;
    int n = 4;
    int waitstat;
    pid_t childpid;

    for ( i=0;  i < n; i++ )  {
        childpid = fork();
        if ( childpid != 0 )  {
            /* I just created a child */
            break;
        }
    }

    wait( &waitstat );
    printf( "Process-D: %-8ld",  (long)getpid() );
    printf( "Parent-Process-ID: %-8ld\n",  (long)getppid() );
}