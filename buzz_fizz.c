#include <stdio.h>

int main() {
// Multiplos de 3 - Fizz
// Multiplos de 5 - Buzz
// Multiplos dos 2 Fizz e Buzz
// Outros, diz o número
    int i;

    for (i = 1; i <= 100; i++) {
        if ((i % 3) == 0 && ((i % 5) != 0))
            printf("O número é %d, Fizz \n", i);
        else if ((i % 5) == 0 && ((i % 3) != 0))
            printf("O número é %d, Buzz \n", i);
        else if (((i % 5 ) == 0) && ((i % 3) == 0))
            printf("O número é %d, FizzBuzz \n", i);
        else
            printf("%d \n", i);
    }

    return 0;
}