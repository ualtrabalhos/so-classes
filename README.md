# Sistemas Operativos - Geral
## Repo para testes e exercicios da aula.

**Como utilizar?**

1. Instalar git -> https://git-scm.com/downloads
2. `git clone git@bitbucket.org:ualtrabalhos/sistemas-operativos-geral.git`
3. Executar programa. Com Atom instalar **gpp-compiler** para compilar e executar, para utilizar basta carregar **F5**

***Exercícios***

1. Conversor Celsius para Fahrenheit
2. Buzz Fizz
3. Guiao 1 

### Links úteis
- Tipos de formatos scanf https://en.wikipedia.org/wiki/Scanf_format_string
- Comandos git https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html
- Tuts C https://www.programiz.com/c-programming/examples
