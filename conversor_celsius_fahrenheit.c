#import <stdio.h>

void celsius_to_far(float celsius){
  float fahrenheit = (celsius * 9/5) + 32;
  printf("Calculando o valor com a formula (celsius * 9/5) + 32, o resultado é: \n");
  printf("%.2fºC = %.2fºF \n", celsius, fahrenheit);
  // printf("Também podemos calcular Celsius * 1.8 + 32, ficando: %.2f \n", celsius * 1.8 + 32);
}

int main(){
    float celsius;
    printf("Coloque um valor em graus Celsius, para converter para Fahrenheit: \n");
    scanf("%f", &celsius); //Colocamos o valor no endereço de memória


    celsius_to_far(celsius); //Passamos o valor e retorna os calculos

    return 0; // Isto faz com que o programa acabe. Não testei se isto se traduz em espaço alocado na memória... 
}
